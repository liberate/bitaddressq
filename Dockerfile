FROM registry.revolt.org/software/containers/chaperone-base:no-masters

RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get -qy install --no-install-recommends bundler ruby

ADD . /bitaddressq
WORKDIR /bitaddressq
RUN bundle install

COPY chaperone.d/ /etc/chaperone.d

ENTRYPOINT ["/usr/local/bin/chaperone"]
