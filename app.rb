require 'sinatra'
require "sinatra/reloader" if development?

require 'rqrcode'
require File.expand_path '../config.rb', __FILE__

helpers do
  #
  # write out the address file safely, with an exclusive file lock.
  #
  def write_addresses(coin_type, addresses)
    return true if ENV['RACK_ENV'] != "production"
    # we can't use "w" because it truncates the file before lock, sometimes messing up the lock.
    File.open(addresses_filename(coin_type), File::RDWR|File::CREAT, 0644) do |f|
      f.flock(File::LOCK_EX)
      f.rewind
      f.write(addresses.join("\n"))
      f.flush
      f.truncate(f.pos)
    end
    return true
  rescue
    return false
  end

  #
  # read addresses safely, with a shared filed lock
  #
  def read_addresses(coin_type)
    addresses = []
    File.open(addresses_filename(coin_type), "r") do |f|
      f.flock(File::LOCK_SH)
      addresses = f.read.split("\n")
    end
    if addresses.empty?
      nil
    else
      addresses
    end
  rescue
    return nil
  end

  def addresses_filename(coin_type)
    ADDRESS_DIRECTORY + COINS[coin_type]["file"]
  end

  def send_email_reminder
    `echo "Time to generate more bitcoin donation addresses in #{ADDRESS_FILE}" | mail -s "bitcoin addresses are running low" #{NOTIFY_EMAIL}`
  end

  def valid_coin_type?(coin_type)
    !!COINS[coin_type]
  end

  def coin_name(coin_type)
    COINS[coin_type]["name"]
  end

end

get '/' do
  erb :form
end

post '/:coin_type' do
  begin
    coin_type = params['coin_type']
    unless valid_coin_type?(coin_type)
      @message = "Bad request"
      erb :error
      return
    end
    addresses = read_addresses(coin_type)
    if addresses
      @coin_name = coin_name(coin_type)
      if ENV['RACK_ENV'] == "production"
        @address = addresses.pop.strip
      else
        @address = addresses.shuffle.pop.strip
      end
      @qr = RQRCode::QRCode.new(@address, :size => 5)
      if write_addresses(coin_type, addresses)
        if addresses.length < LOW_ADDRESS_COUNT
          send_email_reminder
        end
        erb :address
      else
        @message = "Could not access pool of addresses."
        erb :error
      end
    else
      @message = "No more addresses at this time. We will create more soon."
      erb :error
    end
  rescue StandardError => exc
    @message = "Unknown error"
    puts exc
    erb :error
  end
end
